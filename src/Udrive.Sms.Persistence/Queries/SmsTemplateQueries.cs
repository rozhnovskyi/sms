﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udrive.Sms.Application.Contracts.Queries;
using Udrive.Sms.Application.Entities;

namespace Udrive.Sms.Persistence.Queries
{
    public class SmsTemplateQueries : ISmsTemplateQueries
    {
        private readonly SmsDbContext _context;

        public SmsTemplateQueries(SmsDbContext dbContext)
        {
            _context = dbContext;
        }

        public async Task<List<SmsTemplate>> GetAllAsync()
        {
            return await _context.SmsTemplates.AsNoTracking().ToListAsync();
        }

        public async Task<SmsTemplate> GetAsync(int id)
        {
            return await _context.SmsTemplates.AsNoTracking().FirstOrDefaultAsync(c => c.Id == id);
        }
    }
}