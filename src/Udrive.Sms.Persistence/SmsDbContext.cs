﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Udrive.Common;
using Udrive.Common.Contracts;
using Udrive.Sms.Application.Common.Persistence;
using Udrive.Sms.Application.Entities;
using IUnitOfWork = Udrive.Common.Data.IUnitOfWork;

namespace Udrive.Sms.Persistence
{
    public class SmsDbContext : DbContext, IUnitOfWork, IDbContext
    {
        private readonly IUserAccessor _userAccessor;

        public SmsDbContext(IUserAccessor userAccessor)
        {
            _userAccessor = userAccessor;
        }

        public DbSet<SmsHistory> SmsHistories { get; set; }
        public DbSet<SmsTemplate> SmsTemplates { get; set; }
        
        
        
        public SmsDbContext(DbContextOptions<SmsDbContext> options, IUserAccessor userAccessor)
            : base(options)
        {
            _userAccessor = userAccessor;
        }
        
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<SmsHistory>(r =>
            {
                r.ToTable("SmsHistories");
                r.HasKey(e => e.Id);
            });
            
            modelBuilder.Entity<SmsTemplate>(r =>
            {
                r.ToTable("SmsTemplates");
                r.HasKey(e => e.Id);
            });
        }
        
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SetCreatedFields();
            SetUpdatedFields();

            return await base.SaveChangesAsync(cancellationToken);
        }
        
        private void SetCreatedFields()
        {
            foreach (var entity in ChangeTracker.Entries().Where(e => e.State == EntityState.Added)
                .Select(e => e.Entity).OfType<TrackEntity>())
            {
                entity.CreatedBy = _userAccessor.User.Id;
                entity.CreatedOn = DateTime.UtcNow;
            }
        }

        private void SetUpdatedFields()
        {
            foreach (var entity in ChangeTracker.Entries().Where(e => e.State == EntityState.Modified)
                .Select(e => e.Entity).OfType<TrackEntity>())
            {
                entity.UpdatedBy = _userAccessor.User.Id;
                entity.UpdatedOn = DateTime.UtcNow;
            }
        }
        
        
        
    }
} 