﻿using System;
using System;
using AutoMapper.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Udrive.Common.Data;
using Udrive.Sms.Application.Common.Persistence;
using Udrive.Sms.Application.Contracts.Queries;
using Udrive.Sms.Persistence.Queries;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace Udrive.Sms.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SmsDbContext>(options =>
            {
                options.UseNpgsql(
                    configuration.GetConnectionString("DefaultDbConnection"),
                    builder => builder.SetPostgresVersion(new Version("12.0")));
            });

             services.AddScoped<IDbContext>(provider => provider.GetRequiredService<SmsDbContext>());
             services.AddScoped<ISmsTemplateQueries,SmsTemplateQueries>();

             return services;
        }
    }
}