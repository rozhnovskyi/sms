﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Udrive.Sms.Persistence
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<SmsDbContext>
    {
        public SmsDbContext CreateDbContext(string[] args)
        {
            var connectionString = GetConnectionString();

            var optionsBuilder = new DbContextOptionsBuilder<SmsDbContext>();
            optionsBuilder
                .UseNpgsql(
                    connectionString,
                    builder => builder.SetPostgresVersion(new Version("12.0")));

            return new SmsDbContext(optionsBuilder.Options, null);
        }

        private string GetConnectionString()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory() + string.Format("{0}..{0}Udrive.Sms.Api", Path.DirectorySeparatorChar))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.Development.json", optional: true)
                .AddEnvironmentVariables()
                .Build();

            return configuration.GetConnectionString("DefaultDbConnection");
        }
    }
}