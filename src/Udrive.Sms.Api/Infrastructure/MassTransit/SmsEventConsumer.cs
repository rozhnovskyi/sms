﻿using System.Threading.Tasks;
using MassTransit;
using IMediator = MediatR.IMediator;
using Udrive.Common.Events.Sms;
using Udrive.Sms.Application.Commands;

namespace Udrive.Sms.Api.Infrastructure.MassTransit
{
    public class SmsEventConsumer : IConsumer<SmsEvent>
    {
        private readonly IMediator _mediator;
        
        public SmsEventConsumer(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        public async Task Consume(ConsumeContext<SmsEvent> context)
        {
            await _mediator.Send(new SendSmsCommand
            {
                Text = context.Message.SmsText,
                PhoneNumber = context.Message.PhoneNumber,
                SmsType = context.Message.SmsType
            });
        }
 
    }
}