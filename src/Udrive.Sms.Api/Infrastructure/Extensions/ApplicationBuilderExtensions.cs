using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Udrive.Sms.Persistence;

namespace Udrive.Sms.Api.Infrastructure.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void ApplyMigrations(this IApplicationBuilder app, IConfiguration configuration)
        {
            if (!configuration.GetValue<bool>("General:AutoMigrations"))
            {
                return;
            }

            using var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var context = scope.ServiceProvider.GetService<SmsDbContext>();
            context.Database.Migrate();
        }
    }
}