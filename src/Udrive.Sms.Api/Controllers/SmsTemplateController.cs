﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udrive.Common.SearchEngine;
using Udrive.Common.Security;
using Udrive.Sms.Application.Commands;
using Udrive.Sms.Application.Contracts.Queries;
using Udrive.Sms.Application.Entities;

namespace Udrive.Sms.Api.Controllers
{
    [Authorize(Policy = Policies.ManageFleets)]
    [ApiController]
    [Route("api/sms")]
    public class SmsTemplateController : ControllerBase
    {
        private readonly ISmsTemplateQueries _smsTemplateQueries;
        private readonly IMediator _mediator;

        public SmsTemplateController(ISmsTemplateQueries smsTemplateQueries, IMediator mediator)
        {
            _smsTemplateQueries = smsTemplateQueries;
            _mediator = mediator;
        }

        [HttpGet("all")]
        public async Task<List<SmsTemplate>> GetAll()
        {
            return await _smsTemplateQueries.GetAllAsync();
        }
        
        
        [HttpGet]
        public async Task<SmsTemplate> Get(int templateId)
        {
            return await _smsTemplateQueries.GetAsync(templateId);
        }

        [HttpPost]
        public async Task<SmsTemplate> Post([FromBody] CreateSmsTemplateCommand command)
        {
            return await _mediator.Send(command);
        }

    }
} 