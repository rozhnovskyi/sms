using System;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using GreenPipes;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Udrive.Common.AspNetCore;
using Udrive.Common.AspNetCore.Security;
using Udrive.Common.AspNetCore.Swagger;
using Udrive.Common.Constants;
using Udrive.Common.Contracts;
using Udrive.Common.Security;
using Udrive.Sms.Api.Infrastructure.Extensions;
using Udrive.Sms.Api.Infrastructure.MassTransit;
using Udrive.Sms.Application.Commands;
using Udrive.Sms.Persistence;

namespace Udrive.Sms.Api
{
    public class Startup
    {
        private IConfiguration Configuration { get; }

        private IWebHostEnvironment Environment { get; }
       
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddPersistence(Configuration);
            
            services
                .AddAutoMapper(typeof(Startup).Assembly);
            
            services.AddMediatR(typeof(CreateSmsTemplateCommandHandler));

            services.AddControllers();
            services.AddSwagger(Configuration, Environment);
            
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = Configuration.GetValue<string>("Services:Identity:Url");
                    options.Audience = Configuration.GetValue<string>("Services:Identity:DefaultScope");
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters.ValidateIssuer = false;
                });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();

                options.AddPolicy(Policies.ManageFleets, policyBuilder =>
                    policyBuilder
                        .Combine(options.DefaultPolicy)
                        .RequireClaim("permission", Permissions.FleetManagement));
            });
            
            services.AddSingleton<IAuthorizationHandler, FleetAuthorizationHandler>();

            
            services.AddMassTransit(configure =>
                {
                    configure.AddBus(provider => CreateBusControl(provider, Configuration));
                    configure.AddConsumer<SmsEventConsumer>();
                    
                    configure.AddTransactionOutbox();
                })
                .AddMassTransitHostedService();
            
            
            services.AddHttpContextAccessor();
            services.AddSingleton<IUserAccessor>(sp =>
                new UserAccessor(sp.GetRequiredService<IHttpContextAccessor>(), Applications.Payment));

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.ApplyMigrations(Configuration);
            
            var pathBase = Configuration.GetValue<string>("ASPNETCORE_APPL_PATH");
            if (!string.IsNullOrWhiteSpace(pathBase))
            {
                app.UsePathBase(pathBase);
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseSwagger(o => o.SerializeAsV2 = false);
            
            app.UseSwaggerUI(setup =>
            {
                setup.SwaggerEndpoint($"{pathBase}/swagger/v1/swagger.json", $"{Configuration.GetValue<string>("General:ServiceName")} v1");

                if (Environment.IsDevelopment())
                {
                    setup.OAuthClientId("ui.client");
                    setup.OAuthClientSecret("secret");
                }
            });
            
            app.UseRouting();
            
            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", context =>
                {
                    context.Response.Redirect($"{pathBase}/swagger/index.html");
                    return Task.CompletedTask;
                });

                endpoints.MapControllers();
            });
        }
        
        private static IBusControl CreateBusControl(IServiceProvider provider, IConfiguration configuration)
        {
            return Bus.Factory.CreateUsingRabbitMq(configure =>
            {
        
                configure.Host(configuration.GetValue<string>("MassTransit:HostSettings:Host"), "/", configurator =>
                {
                    configurator.Username(configuration.GetValue<string>("MassTransit:HostSettings:Username"));
                    configurator.Password(configuration.GetValue<string>("MassTransit:HostSettings:Password"));
                });
                
                configure.UseConcurrencyLimit(5);
                configure.UseRetry(r =>
                {
                    r.Interval(3, TimeSpan.FromSeconds(2));
                });
        
                configure.ReceiveEndpoint(
                    configuration.GetValue<string>("General:ServiceName"),
                    configurator =>
                    {
                        configurator.Consumer<SmsEventConsumer>(provider);
                    });
            });
        }
        
    }
}