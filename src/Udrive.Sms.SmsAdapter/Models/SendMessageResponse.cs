﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Udrive.Sms.SmsAdapter.Models
{
    public class SendMessageResponse
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        //[JsonProperty("error_code")]
        //public int ErrorCode { get; set; }

        //[JsonProperty("error")]
        //public string ErrorDescription { get; set; }

        [JsonProperty("cnt")]
        public int Quantity { get; set; }

        [JsonProperty("cost")]
        public decimal Cost { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        [JsonProperty("phones")]
        public List<MessagePhoneResponse> Phones { get; set; }

        public ErrorResponse ResponseError { get; set; }

        public SendMessageResponse()
        {
            Phones = new List<MessagePhoneResponse>();
        }
    }
}