﻿using System;
using Newtonsoft.Json;
using Udrive.Sms.SmsAdapter.Utils;

namespace Udrive.Sms.SmsAdapter.Models
{
    public class ErrorResponse
    {
        private string _errorDescription;

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("error_code")]
        public int ErrorCode { get; set; }

        public string ErrorDescription
        {
            get
            {
                if (_errorDescription != null) return _errorDescription;
                return _errorDescription = ErrorDescriptor.GetDescription(this);
            }
        }
    }
}