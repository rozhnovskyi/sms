﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Udrive.Sms.SmsAdapter.Models;

namespace SmsCenter.Entities
{
    public class MessageStatusListResponse
    {
        public List<MessageStatusResponse> Statuses { get; set; }

        public ErrorResponse ResponseError { get; set; }

        public MessageStatusListResponse()
        {
            Statuses = new List<MessageStatusResponse>();
        }
    }
}
