﻿using System;
using Newtonsoft.Json;
using SmsCenter.Enums;
using SmsCenter.Utils;
using Udrive.Sms.SmsAdapter.Models;

namespace SmsCenter.Entities
{
    public class MessageStatusResponse : MessageResponse
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("status_name")]
        public string StatusName { get; set; }

        [JsonProperty("err")]
        public override int ErrorCode { get; set; }

        [JsonProperty("last_timestamp")]
        public long StatusTimeStamp { get; set; }
        public DateTime StatusDate => StatusTimeStamp.GetDateTime();

        [JsonProperty("send_timestamp")]
        public long SendTimeStamp { get; set; }
        public DateTime SendDate => SendTimeStamp.GetDateTime();

        [JsonProperty("sender_id")]
        public string Sender { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }

        [JsonProperty("type")]
        public MessageType MessageType { get; set; }

        public ErrorResponse ResponseError { get; set; }
    }
}