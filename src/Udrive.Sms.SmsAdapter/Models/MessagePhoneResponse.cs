﻿using Newtonsoft.Json;

namespace Udrive.Sms.SmsAdapter.Models
{
    public class MessagePhoneResponse : MessageResponse
    {
        public string Mccmnc { get; set; }

        [JsonProperty("error")]
        public override int ErrorCode { get; set; }
    }
}