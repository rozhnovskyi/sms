﻿using Newtonsoft.Json;
using SmsCenter.Utils;
using Udrive.Sms.SmsAdapter.Enums;
using Udrive.Sms.SmsAdapter.Utils;

namespace Udrive.Sms.SmsAdapter.Models
{
    public abstract class MessageResponse
    {
        private string _phone;
        private string _errorDescription;

        [JsonProperty("status")]
        public MessageStatus? Status { get; set; }

        public abstract int ErrorCode { get; set; }

        public string ErrorDescription
        {
            get
            {
                if (_errorDescription != null) return _errorDescription;
                return _errorDescription = ErrorDescriptor.GetDescription(this);
            }
        }

        [JsonProperty("phone")]
        public string Phone
        {
            get { return _phone; }
            set
            {
                if (value == _phone) return;
                _phone = PhoneConverter.Normalize(value);
            }
        }

        [JsonProperty("cost")]
        public decimal Cost { get; set; }
    }
}