﻿using System.ComponentModel.DataAnnotations;

namespace Udrive.Sms.SmsAdapter.Enums
{
    public enum MessageStatus
    {
        [Display(Name = "Сообщение не найдено")]
        NotFound = -3,

        [Display(Name = "Ожидает отправки")]
        WaitingForSending = -1,

        [Display(Name = "Передано оператору")]
        TransferredToOperator = 0,

        [Display(Name = "Доставлено")]
        Delivered = 1,

        [Display(Name = "Прочитано")]
        Read = 2,

        [Display(Name = "Просрочено")]
        PastDue = 3,

        [Display(Name = "Невозможно доставить")]
        ImpossibleToDeliver = 20,

        [Display(Name = "Неверный номер")]
        WrongNumber = 22,

        [Display(Name = "Запрещено")]
        Forbidden = 23,

        [Display(Name = "Недостаточно средств")]
        InsufficientFunds = 24,

        [Display(Name = "Недоступный номер")]
        UnavailableNumber = 25
    }
}