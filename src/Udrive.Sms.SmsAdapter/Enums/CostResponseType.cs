﻿namespace Udrive.Sms.SmsAdapter.Enums
{
    public enum CostResponseType
    {
        None = 0,
        OnlyCostWithoutSend = 1,
        Cost = 2,
        CostAndBalance = 3    
    }
}