﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmsCenter.Enums
{
    public enum MessageType
    {
        Sms = 0,
        FlashSms = 1,
        BinarySms = 2,
        WapPush = 3,
        HlrRequest = 4,
        PingSms = 5,
        Mms = 6,
        Call = 7,
        Viber = 10
    }
}
