﻿using System;
using System.Collections.Specialized;
using Udrive.Sms.SmsAdapter.Enums;

namespace SmsCenter
{
    public class MessageSenderOptions
    {
        public string Login { get; }
        public string Password { get; }

        public TranslitType Translit { get; set; }

        public bool TinyUrl { get; set; }

        public string CharSet { get; set; }

        public CostResponseType Cost { get; set; }

        public int SendMessageResponseFormat => 3;

        public int LifeTime { private get; set; }

        public string Validity
        {
            get
            {
                var hours = (int) Math.Floor(LifeTime / 60M);
                var minutes = LifeTime - hours * 60;

                return new TimeSpan(hours, minutes, 0).ToString("hh\\:mm");
            }
        }

        public bool DetailedSendMessageResponse { get; set; }

        public bool DetailedSendMessageErrorResponse => true;

        public bool Viber { get; set; }

        public int GetStatusResponseFormat => 2;

        public void SetDefaults()
        {
            Translit = TranslitType.None;
            TinyUrl = true;
            CharSet = "utf-8";
            Cost = CostResponseType.CostAndBalance;
            LifeTime = 60;
            DetailedSendMessageResponse = true;
            Viber = true;
        }

        public MessageSenderOptions(string login, string password) : this()
        {
            Login = login;
            Password = password;
        }

        private MessageSenderOptions()
        {
            SetDefaults();
        }
    }
}