﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using SmsCenter;
using SmsCenter.Entities;
using SmsCenter.Utils;
using Udrive.Sms.SmsAdapter.Models;

namespace Udrive.Sms.SmsAdapter
{
    public class MessageSender
    {
        private const string SendMessageUrl = "https://smsc.ua/sys/send.php";
        private const string GetMessageStatusUrl = "https://smsc.ua/sys/status.php";

        private readonly MessageSenderOptions _options;
        private readonly WebClient _webClient;
        
        public MessageSender(MessageSenderOptions options)
        {
            _options = options;
            _webClient = new WebClient();
        }
        
        [Obsolete("Выяснить у разработчиков алгоритм хеширования")]
        private static string GetMd5Hash(string source)
        {
            var hashArray = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(source));

            var result = new StringBuilder();

            foreach (var hashByte in hashArray)
            {
                result.Append(hashByte.ToString("X2"));
            }
            return result.ToString().ToLower();
        }

        private SendMessageResponse SendMessage(string phones, string message, Guid messageId)
        {
            var postData = new RequestParamsBuilder(_options)
                .Login()
                .SendMessage(phones, message, messageId.ToString())
                .Build();

            var byteResponse = _webClient.UploadValues(SendMessageUrl, postData);

            SendMessageResponse response; 

            try
            {
                response = JsonConvert.DeserializeObject<SendMessageResponse>(Encoding.UTF8.GetString(byteResponse),
                    new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
            }
            catch (Exception)
            {
                response = new SendMessageResponse();
            }

            response.ResponseError = JsonConvert.DeserializeObject<SendMessageErrorResponse>(Encoding.UTF8.GetString(byteResponse));

            return response;
        }

        public SendMessageResponse Send(string[] phones, string message, Guid? messageId = null)
        {
            return SendMessage(string.Join(";", phones), message, messageId ?? Guid.NewGuid());
        }

        public SendMessageResponse Send(string phone, string message, Guid? messageId = null)
        {
            return SendMessage(phone, message, messageId ?? Guid.NewGuid());
        }

        private MessageStatusListResponse GetStatus(string phones, string messageIds)
        {
            var postData = new RequestParamsBuilder(_options).Login().GetStatus(phones, messageIds).Build();

            var byteResponse = _webClient.UploadValues(GetMessageStatusUrl, postData);

            var response = new MessageStatusListResponse();

            try
            {
                response.Statuses = JsonConvert.DeserializeObject<List<MessageStatusResponse>>(Encoding.UTF8.GetString(byteResponse));
            }
            catch (Exception)
            {
            }

            return response;
        }

        public MessageStatusListResponse GetStatus(string phone, Guid messageId)
        {
            return GetStatus(phone + ",", messageId + ",");
        }

        public MessageStatusListResponse GetStatus(string[] phones, Guid[] messageIds)
        {
            return GetStatus(string.Join(",", phones) + ",", string.Join(",", messageIds) + ",");
        }

    }
}