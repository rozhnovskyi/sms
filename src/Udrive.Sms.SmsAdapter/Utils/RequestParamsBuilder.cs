﻿﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmsCenter.Utils
{
    public class RequestParamsBuilder
    {
        private MessageSenderOptions _options;
        private NameValueCollection _params;

        public RequestParamsBuilder Login()
        {
            _params.Add("login", _options.Login);
            _params.Add("psw", _options.Password);

            return this;
        }

        public RequestParamsBuilder SendMessage(string phones, string message, string messageId)
        {

            _params.Add("translit", ((int)_options.Translit).ToString());
            _params.Add("tinyurl", _options.TinyUrl ? "1" : "0");
            _params.Add("cost", ((int)_options.Cost).ToString());
            _params.Add("valid", _options.Validity);
            _params.Add("op", _options.DetailedSendMessageResponse ? "1" : "0");
            _params.Add("err", _options.DetailedSendMessageErrorResponse ? "1" : "0");
            _params.Add("viber", _options.Viber ? "1" : "0");

            _params.Add("phones", phones);
            _params.Add("mes", message);
            _params.Add("id", messageId);

            return this;
        }

        public RequestParamsBuilder GetStatus(string phones, string messageId)
        {
            _params.Add("all", "2");
            _params.Add("phone", phones);
            _params.Add("id", messageId);

            return this;
        }

        public NameValueCollection Build()
        {
            return _params;
        }

        private void SetDefaults()
        {
            _params.Add("charset", _options.CharSet);
            _params.Add("fmt", _options.SendMessageResponseFormat.ToString());
        }

        public RequestParamsBuilder(MessageSenderOptions options)
        {
            _options = options;
            _params = new NameValueCollection();
            SetDefaults();
        }
    }
}
