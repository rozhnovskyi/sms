﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmsCenter.Utils
{
    public static class PhoneConverter
    {
        public static string Normalize(string input)
        {
            var onlyDigits = new string(input.Where(c => char.IsDigit(c) || c == '+').ToArray());

            switch (onlyDigits.Length)
            {
                case 9:
                    return string.Concat("+380", onlyDigits);
                case 10:
                    return string.Concat("+38", onlyDigits);
                case 12:
                    return string.Concat("+", onlyDigits);
                default:
                    return onlyDigits;
            }
        }
    }
}
