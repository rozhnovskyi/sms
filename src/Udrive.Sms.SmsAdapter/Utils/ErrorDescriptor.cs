﻿using System.Collections.Generic;
using SmsCenter.Entities;
using Udrive.Sms.SmsAdapter.Models;

namespace Udrive.Sms.SmsAdapter.Utils
{
    public static class ErrorDescriptor
    {
         private static Dictionary<int, string> _messageErrors = new Dictionary<int, string>
        {
            {0, "Нет ошибки"},
            {1, "Абонент не существует"},
            {6, "Абонент не в сети"},
            {11, "Нет услуги SMS"},
            {13, "Абонент заблокирован"},
            {21, "Нет поддержки SMS"},
            {200, "Виртуальная отправка"},
            {220, "Переполнена очередь у оператора"},
            {240, "Абонент занят"},
            {241, "Ошибка конвертации звука"},
            {242, "Зафиксирован автоответчик"},
            {243, "Не заключен договор"},
            {244, "Рассылки запрещены"},
            {245, "Статус не получен"},
            {246, "Ограничение по времени"},
            {247, "Превышен лимит сообщений"},
            {248, "Нет маршрута"},
            {249, "Неверный формат номера"},
            {250, "Номер запрещен настройками"},
            {251, "Превышен лимит на один номер"},
            {252, "Номер запрещен"},
            {253, "Запрещено спам-фильтром"},
            {254, "Незарегистрированный sender id"},
            {255, "Отклонено оператором"}
        };

        private static Dictionary<int, string> _responseErrors = new Dictionary<int, string>
        {
            {1, "Ошибка в параметрах."},
            {2, "Неверный логин или пароль."},
            {4, "IP-адрес временно заблокирован."},
            {5, "Ошибка удаления сообщения."},
            {9, "Попытка отправки более пяти запросов на получение статуса одного и того же сообщения в течение минуты."}
        };

        private static Dictionary<int, string> _sendMessageResponseErrors = new Dictionary<int, string>
        {
            {1, "Ошибка в параметрах."},
            {2, "Неверный логин или пароль."},
            {3, "Недостаточно средств на счете Клиента."},
            {4, "IP-адрес временно заблокирован из-за частых ошибок в запросах. Подробнее"},
            {5, "Неверный формат даты."},
            {6, "Сообщение запрещено (по тексту или по имени отправителя)."},
            {7, "Неверный формат номера телефона."},
            {8, "Сообщение на указанный номер не может быть доставлено."},
            {9, "Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты."}
        };

        private static string GetDescription(Dictionary<int, string> errors, int errorCode)
        {
            string description;
            errors.TryGetValue(errorCode, out description);

            return description ?? $"Код ошибки {errorCode}: описание отсутствует";
        }

        public static string GetDescription(MessageResponse message)
        {
            return GetDescription(_messageErrors, message.ErrorCode);
        }

        public static string GetDescription(ErrorResponse response)
        {
            if (response is SendMessageErrorResponse)
                return GetDescription(_sendMessageResponseErrors, response.ErrorCode);
            else
                return GetDescription(_responseErrors, response.ErrorCode);
        }
    }
}