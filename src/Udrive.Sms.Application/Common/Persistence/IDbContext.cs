﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udrive.Sms.Application.Entities;

namespace Udrive.Sms.Application.Common.Persistence
{
    public interface IDbContext
    {
        public DbSet<SmsHistory> SmsHistories { get; set; }
        public DbSet<SmsTemplate> SmsTemplates { get; set; }
        
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}