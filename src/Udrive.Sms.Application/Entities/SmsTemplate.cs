﻿using Udrive.Common;

namespace Udrive.Sms.Application.Entities
{
    public class SmsTemplate : BaseEntity<int>
    {
        public int TemplateType { get; set; }
        public string TemplateText { get; set; }
    }
}