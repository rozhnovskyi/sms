﻿using System;
using Udrive.Common;

namespace Udrive.Sms.Application.Entities
{
    public class SmsHistory: BaseEntity<int>
    {
        public string SmsText { get; set; }
        public string PhoneNumber { get; set; }
        public string Status { get; set; }
        public DateTime SendDateTime { get; set; }
    }
}