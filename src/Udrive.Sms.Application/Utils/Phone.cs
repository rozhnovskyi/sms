﻿using System.Linq;

namespace Udrive.Sms.Application.Utils
{
    public static class Phone
    {
        public static string NormalizePhone(string input)
        {
            if (string.IsNullOrEmpty(input))
                return input;

            var onlyDigits = new string(input.Where(c => char.IsDigit(c)).ToArray());

            switch (onlyDigits.Length)
            {
                case 9:
                    return string.Concat("+380", onlyDigits.ToString());
                case 10:
                    return string.Concat("+38", onlyDigits.ToString());
                case 12:
                    return string.Concat("+", onlyDigits.ToString());
                default:
                    return onlyDigits;
            }
        }
    }
}