﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Udrive.Sms.Application.Entities;

namespace Udrive.Sms.Application.Contracts.Queries
{
    public interface ISmsTemplateQueries
    {
        Task<List<SmsTemplate>> GetAllAsync();
        
        Task<SmsTemplate> GetAsync(int id);
    }
}