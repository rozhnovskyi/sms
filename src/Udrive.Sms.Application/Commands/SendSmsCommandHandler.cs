﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SmsCenter;
using Udrive.Common.Dto;
using Udrive.Common.Events.Sms;
using Udrive.Sms.Application.Common.Persistence;
using Udrive.Sms.Application.Entities;
using Udrive.Sms.Application.Utils;
using Udrive.Sms.SmsAdapter;

namespace Udrive.Sms.Application.Commands
{
    public class SendSmsCommandHandler : AsyncRequestHandler<SendSmsCommand>
    {
        private const string Login = "uberdrive";
        private const string Password = "WgPcs-9D+v";
        
        private readonly IDbContext _dbContext;

        public SendSmsCommandHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        protected override async Task Handle(SendSmsCommand request, CancellationToken cancellationToken)
        {
            var smsTemplate = _dbContext.SmsTemplates.FirstOrDefault(s => s.TemplateType == request.SmsType);
            var messageId = Guid.NewGuid();
            var phoneNumber = Phone.NormalizePhone(request.PhoneNumber);
            var smsText = string.Concat(smsTemplate?.TemplateText, " ", request.Text);
            var sender = new MessageSender(new MessageSenderOptions(Login, Password));
            var response = sender.Send(phoneNumber, smsText , messageId);
            var errorDescription = response.ResponseError.ErrorDescription;
                _dbContext.SmsHistories.Add(new SmsHistory
                {
                    Status = errorDescription,
                    PhoneNumber = phoneNumber,
                    SmsText = smsText
                });
                await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}