﻿using MediatR;
using Udrive.Sms.Application.Entities;

namespace Udrive.Sms.Application.Commands
{
    public class CreateSmsTemplateCommand : IRequest<SmsTemplate>
    {
        public int TemplateType { get; set; }
        public string TemplateText { get; set; }
    }
}