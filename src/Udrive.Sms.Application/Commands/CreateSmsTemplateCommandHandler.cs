﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Udrive.Sms.Application.Common.Persistence;
using Udrive.Sms.Application.Entities;

namespace Udrive.Sms.Application.Commands
{
    public class CreateSmsTemplateCommandHandler : IRequestHandler<CreateSmsTemplateCommand, SmsTemplate>
    {
        private readonly IDbContext _dbContext;

        public CreateSmsTemplateCommandHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<SmsTemplate> Handle(CreateSmsTemplateCommand request, CancellationToken cancellationToken)
        {
            var smsTemplate = _dbContext.SmsTemplates.FirstOrDefault(t => t.TemplateType == request.TemplateType);
            if (smsTemplate != null)
                smsTemplate.TemplateText = request.TemplateText;
            else 
                _dbContext.SmsTemplates.Add(new SmsTemplate
                {
                    TemplateText = request.TemplateText,
                    TemplateType = request.TemplateType
                });

            await _dbContext.SaveChangesAsync(cancellationToken);
            
            return smsTemplate ?? new SmsTemplate {
                TemplateText = request.TemplateText,
                TemplateType = request.TemplateType
            };
        }
    }
}