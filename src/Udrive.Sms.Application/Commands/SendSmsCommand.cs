﻿using MediatR;
using Udrive.Common.Dto;

namespace Udrive.Sms.Application.Commands
{
    public class SendSmsCommand : IRequest
    {
        public int SmsType { get; set; }
        public string Text { get; set; }
        public string PhoneNumber { get; set; }
    }
}